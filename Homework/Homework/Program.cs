﻿using Common;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Homework;

public class Program
{
    private static void Main(string[] args)
    {
        var provider = DependencyInjection.ConfigureServices();
        var service = provider.GetRequiredService<IPrototypePatternService>();
        service.Process();
    }
}