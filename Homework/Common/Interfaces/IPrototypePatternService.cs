﻿namespace Common.Interfaces;

public interface IPrototypePatternService
{
    void Process();
}