﻿using Common.Interfaces;

namespace Common.Implementations;

public class CargoCar : Car, IMyCloneableCar<ICar>, ICloneable
{
    public CargoCar(int id, string? name, int capacity) : base(id, name)
    {
        Capacity = capacity;
    }

    public int Capacity { get; }

    object ICloneable.Clone()
    {
        return Clone();
    }

    public void GetInfo()
    {
        Console.WriteLine($"Id:{Id}, Name:{Name}, Capacity:{Capacity}");
    }

    public IMyCloneableCar<ICar> Clone()
    {
        return new CargoCar(Id, Name, Capacity);
    }
}